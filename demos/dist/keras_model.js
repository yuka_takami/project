function mnist(){

      var digit = new Float32Array(28*28);
      var sample = new Array(1);
      sample[0] = new Array(28);
			for(let x = 0; x < 28; x++) {
  			sample[0][x] = new Array(28).fill(0);
      }
      for(let x = 0; x < 28; x++){
        for(let y = 0; y < 28; y++){
		sample[0][x][y] = new Array(1).fill(0);
	}
      }

      var canvas = document.getElementById('Canvas');
      var context= canvas.getContext('2d');
			var canvas2 = document.getElementById('Canvas2');
      var context2= canvas2.getContext('2d');

      var imageData = context.getImageData(0, 0, 280, 280);
      var imageData2 = context2.createImageData(28,28);
		 
      var k = 0;
      var l = 0;
      for (i=0; i<280; i=i+10){
       for (j=0; j<280; j=j+10){
        k = 280*i+j;
        var r = imageData.data[k*4+0]; //R
        var g = imageData.data[k*4+1]; //G
        var b = imageData.data[k*4+2]; //B
        var gray = (r+g+b)/3;
        imageData2.data[l*4+0] = gray;
        imageData2.data[l*4+1] = gray;
        imageData2.data[l*4+2] = gray;
        imageData2.data[l*4+3] = 0xFF; //�P�x
	digit[l]=gray/255.0;
        l=l+1;
       }
      }
      context2.putImageData(imageData2, 0, 0);
 
      _s = "./mnist_cnn.bin"

      const model = new KerasJS.Model({
       filepath: _s,
       gpu: false
      })
  
     model
      .ready()
      .then(() => {
         const inputData = {
          input: new Float32Array(digit)
         }
         return model.predict(inputData)
      })
     .then(outputData => {
       console.log('output: '+outputData.output[0]+','+outputData.output[1]
	+','+outputData.output[2]+','+outputData.output[3]+','+outputData.output[4]
	+','+outputData.output[5]+','+outputData.output[6]+','+outputData.output[7]
	+','+outputData.output[8]+','+outputData.output[9]);
      })
      .catch(err => {
       console.log('error: '+err);
      })

}
