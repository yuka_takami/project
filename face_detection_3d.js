let videoWidth, videoHeight

// whether streaming video from the camera.
let streaming = false

let video = document.getElementById('video')
let canvasOutput = document.getElementById('canvasOutput')
let canvasOutputCtx = canvasOutput.getContext('2d')
let stream = null
let canvasOutput2 = document.getElementById('canvasOutput2')
let canvasOutput2Ctx = canvasOutput2.getContext('2d')
let canvasOutput9 = document.getElementById('canvasOutput9')
let canvasOutput9Ctx = canvasOutput9.getContext('2d')
let detectFace = document.getElementById('face')
let detectEye = document.getElementById('eye')

document.getElementById('canvasOutput2').style.visibility = 'hidden'
document.getElementById('canvasOutput3').style.visibility = 'hidden'
document.getElementById('canvasOutput4').style.visibility = 'hidden'
document.getElementById('canvasOutput5').style.visibility = 'hidden'
document.getElementById('canvasOutput6').style.visibility = 'hidden'
document.getElementById('canvasOutput7').style.visibility = 'hidden'
document.getElementById('canvasOutput8').style.visibility = 'hidden'
document.getElementById('canvasOutput9').style.visibility = 'hidden'

document.getElementById('download_button').style.visibility = 'hidden'

//document.getElementById('reset_button').onclick = function () {
//  if (window.confirm("リセットしますか？")) {
//    canvasOutput9Ctx.clearRect(0, 0, canvasOutput9.width, canvasOutput9.height)
//  }
//}

let leftface = []
let leftrighteye = []
let leftlefteye = []
let rightface = []
let rightrighteye = []
let rightlefteye = []
var face_grid_left = []
var face_grid_right = []

function startCamera() {
  if (streaming) return
  navigator.mediaDevices
    .getUserMedia({ video: true, audio: false })
    .then(function (s) {
      stream = s
      video.srcObject = s
      video.play()
    })
    .catch(function (err) {
      console.log('An error occured! ' + err)
    })

  video.addEventListener(
    'canplay',
    function (ev) {
      if (!streaming) {
        videoWidth = video.videoWidth;
        videoHeight = video.videoHeight;
        video.setAttribute('width', videoWidth)
        video.setAttribute('height', videoHeight)
        canvasOutput.width = videoWidth
        canvasOutput.height = videoHeight
        streaming = true
      }
      startVideoProcessing()
    },
    false
  )
}

let faceClassifier = null
let eyeClassifier = null

let src = null
let dstC1 = null
let dstC3 = null
let dstC4 = null

let canvasInput = null
let canvasInputCtx = null

let canvasBuffer = null
let canvasBufferCtx = null

function startVideoProcessing() {
  if (!streaming) {
    console.warn('Please startup your webcam')
    return
  }
  stopVideoProcessing()
  canvasInput = document.createElement('canvas')
  canvasInput.width = videoWidth
  canvasInput.height = videoHeight
  canvasInputCtx = canvasInput.getContext('2d')

  canvasBuffer = document.createElement('canvas')
  canvasBuffer.width = videoWidth
  canvasBuffer.height = videoHeight
  canvasBufferCtx = canvasBuffer.getContext('2d')

  srcMat = new cv.Mat(videoHeight, videoWidth, cv.CV_8UC4)
  grayMat = new cv.Mat(videoHeight, videoWidth, cv.CV_8UC1)

  faceClassifier = new cv.CascadeClassifier()
  faceClassifier.load('haarcascade_frontalface_default.xml')

  eyeClassifier = new cv.CascadeClassifier()
  eyeClassifier.load('haarcascade_eye.xml')

  setInterval('requestAnimationFrame(processVideo)', 5000)
}

function processVideo() {
  let canvaslist = []
  console.log('bigin')
  //stats.begin()
  canvasInputCtx.drawImage(video, 0, 0, videoWidth, videoHeight)
  let imageData = canvasInputCtx.getImageData(0, 0, videoWidth, videoHeight)

  //Save the photograph.///
  let src = cv.matFromImageData(imageData)
  document.getElementById('canvasOutput2').style.visibility = 'visible'
  cv.imshow('canvasOutput2', src)
  //console.log(src.channels()); ->4 channels=RGBA
  /////////////////////////
  var l_face = []
  var l_r_eye = []
  var l_l_eye = []
  var r_face = []
  var r_r_eye = []
  var r_l_eye = []

  var tmp_x = new Array(2) // x of the faces
  var tmp_face = new Array(2)
  var tmp_r_eye = new Array(2)
  var tmp_l_eye = new Array(2)
  var eye_x = new Array(2)
  var both_eye = new Array(2)

  var faceVect

  var flag = 0

  srcMat.data.set(imageData.data)
  var faces = []
  var eyes = []
  var grids_left = []
  var grids_right = []
  for (let m = 0; m < 25; m++) {
    grids_left[m] = []
    grids_right[m] = []
    for (let n = 0; n < 25; n++) {
      grids_left[m][n] = 0
      grids_right[m][n] = 0
    }
  }

  var size
  faceVect = new cv.RectVector()
  var faceMat = new cv.Mat()
  cv.pyrDown(srcMat, faceMat)
  size = faceMat.size()

  faceClassifier.detectMultiScale(faceMat, faceVect)

  for (let t = 0; t < faceVect.size(); t++) {
    var face = faceVect.get(t)
    faces.push(new cv.Rect(face.x, face.y, face.width, face.height))
    var eyeVect = new cv.RectVector()
    var eyeMat = faceMat.roi(face)

    tmp_x[t] = face.x
    tmp_face[t] = eyeMat

    eyeClassifier.detectMultiScale(eyeMat, eyeVect)
    for (let i = 0; i < eyeVect.size(); i++) {
      var eye = eyeVect.get(i)
      eyes.push(new cv.Rect(face.x + eye.x, face.y + eye.y, eye.width, eye.height))
      if (eyeVect.size() == 2) {
        flag = flag + 1
        both_eye[i] = eyeMat.roi(eyeVect.get(i))
        eye_x[i] = eye.x
      }
    }
    if (eyeVect.size() == 2) {
      if (eye_x[0] < eye_x[1]) {
        tmp_l_eye[t] = both_eye[0]
        tmp_r_eye[t] = both_eye[1]
      } else {
        tmp_l_eye[t] = both_eye[1]
        tmp_r_eye[t] = both_eye[0]
      }
    }

    var face_x_1 = (face.x * 25) / 240
    var face_y_1 = (face.y * 25) / 320
    var face_x_2 = ((face.x + face.width) * 25) / 240
    var face_y_4 = ((face.y + face.height) * 25) / 320

    //切り捨て
    var x1 = Math.floor(face_x_1)
    var y1 = Math.floor(face_y_1)
    //切り上げ
    var x2 = Math.min(24, Math.ceil(face_x_2))
    var y4 = Math.min(24, Math.ceil(face_y_4))

    for (m = y1; m < y4 + 1; m++) {
      for (n = x1; n < x2 + 1; n++) {
        if (t == 0) {
          //一人目
          grids_left[m][n] = 1
        } else {
          //二人目
          grids_right[m][n] = 1
        }
      }
    }
  } //for(t)

  if (flag == 4) {
    if (tmp_x[0] < tmp_x[1]) {
      l_face = tmp_face[0]
      l_r_eye = tmp_r_eye[0]
      l_l_eye = tmp_l_eye[0]
      r_face = tmp_face[1]
      r_r_eye = tmp_r_eye[1]
      r_l_eye = tmp_l_eye[1]
    } else {
      l_face = tmp_face[1]
      l_r_eye = tmp_r_eye[1]
      l_l_eye = tmp_l_eye[1]
      r_face = tmp_face[0]
      r_r_eye = tmp_r_eye[0]
      r_l_eye = tmp_l_eye[0]

      var kari = grids_left
      grids_left = grids_right
      grids_right = kari
    }
    var face_grid_left = []
    var face_grid_right = []
    for (var m = 0; m < 25; m++) {
      face_grid_left = face_grid_left.concat(grids_left[m])
      face_grid_right = face_grid_right.concat(grids_right[m])
    }
  } //if(flag==4)

  canvasOutputCtx.drawImage(canvasInput, 0, 0, videoWidth, videoHeight)
  drawResults(canvasOutputCtx, faces, 'red', size)
  drawResults(canvasOutputCtx, eyes, 'yellow', size)

  if (flag == 4) {
    canvaslist.push[src]
    var dst_lf = new cv.Mat()
    var dst_rf = new cv.Mat()
    var dst_lle = new cv.Mat()
    var dst_lre = new cv.Mat()
    var dst_rle = new cv.Mat()
    var dst_rre = new cv.Mat()
    var dsize = new cv.Size(64, 64)

    //      console.dir(l_face);
    cv.resize(l_face, dst_lf, dsize, 0, 0, cv.INTER_AREA)
    cv.resize(r_face, dst_rf, dsize, 0, 0, cv.INTER_AREA)
    cv.resize(l_l_eye, dst_lle, dsize, 0, 0, cv.INTER_AREA)
    cv.resize(l_r_eye, dst_lre, dsize, 0, 0, cv.INTER_AREA)
    cv.resize(r_l_eye, dst_rle, dsize, 0, 0, cv.INTER_AREA)
    cv.resize(r_r_eye, dst_rre, dsize, 0, 0, cv.INTER_AREA)
    document.getElementById('canvasOutput3').style.visibility = 'visible'
    document.getElementById('canvasOutput4').style.visibility = 'visible'
    document.getElementById('canvasOutput5').style.visibility = 'visible'
    document.getElementById('canvasOutput6').style.visibility = 'visible'
    document.getElementById('canvasOutput7').style.visibility = 'visible'
    document.getElementById('canvasOutput8').style.visibility = 'visible'
    cv.imshow('canvasOutput3', dst_lf)
    cv.imshow('canvasOutput4', dst_rf)
    cv.imshow('canvasOutput5', dst_lle)
    cv.imshow('canvasOutput6', dst_lre)
    cv.imshow('canvasOutput7', dst_rle)
    cv.imshow('canvasOutput8', dst_rre)
    cv.imshow('canvasOutput9', src)
    //canvasOutput9Ctx.drawImage(src, 0, 0)
    canvasOutput9_src = src
    flag = 0
    console.log('end')

    let i = 0
    let k = 64 * 64
    while (i < 64 * 64) {
      leftface[i] = dst_lf.data[i * 4] / 255.0
      rightface[i] = dst_rf.data[i * 4] / 255.0
      leftlefteye[i] = dst_lle.data[i * 4] / 255.0
      leftrighteye[i] = dst_lre.data[i * 4] / 255.0
      rightlefteye[i] = dst_rle.data[i * 4] / 255.0
      rightrighteye[i] = dst_rre.data[i * 4] / 255.0
      leftface[i + k] = dst_lf.data[i * 4 + 1] / 255.0
      rightface[i + k] = dst_rf.data[i * 4 + 1] / 255.0
      leftlefteye[i + k] = dst_lle.data[i * 4 + 1] / 255.0
      leftrighteye[i + k] = dst_lre.data[i * 4 + 1] / 255.0
      rightlefteye[i + k] = dst_rle.data[i * 4 + 1] / 255.0
      rightrighteye[i + k] = dst_rre.data[i * 4 + 1] / 255.0
      leftface[i + k * 2] = dst_lf.data[i * 4 + 2] / 255.0
      rightface[i + k * 2] = dst_rf.data[i * 4 + 2] / 255.0
      leftlefteye[i + k * 2] = dst_lle.data[i * 4 + 2] / 255.0
      leftrighteye[i + k * 2] = dst_lre.data[i * 4 + 2] / 255.0
      rightlefteye[i + k * 2] = dst_rle.data[i * 4 + 2] / 255.0
      rightrighteye[i + k * 2] = dst_rre.data[i * 4 + 2] / 255.0
      i++
    }

    leftrighteye_tensor = tf.tensor4d(leftrighteye, [1, 3, 64, 64], 'float32')
    leftlefteye_tensor = tf.tensor4d(leftlefteye, [1, 3, 64, 64], 'float32')
    leftface_tensor = tf.tensor4d(leftface, [1, 3, 64, 64], 'float32')
    face_grid_left_tensor = tf.tensor4d(face_grid_left, [1, 1, 25, 25], 'float32')
    rightrighteye_tensor = tf.tensor4d(rightrighteye, [1, 3, 64, 64], 'float32')
    rightlefteye_tensor = tf.tensor4d(rightlefteye, [1, 3, 64, 64], 'float32')
    rightface_tensor = tf.tensor4d(rightface, [1, 3, 64, 64], 'float32')
    face_grid_right_tensor = tf.tensor4d(face_grid_right, [1, 1, 25, 25], 'float32')

    tensorflow(
      leftrighteye_tensor,
      leftlefteye_tensor,
      leftface_tensor,
      face_grid_left_tensor,
      rightrighteye_tensor,
      rightlefteye_tensor,
      rightface_tensor,
      face_grid_right_tensor
    )
  } else {
    flag = 0
  }
  requestAnimationFrame(processVideo)
}

function drawResults(ctx, results, color, size) {
  for (let i = 0; i < results.length; ++i) {
    var rect = results[i]
    var xRatio = videoWidth / size.width
    var yRatio = videoHeight / size.height
    ctx.lineWidth = 3
    ctx.strokeStyle = color
    ctx.strokeRect(rect.x * xRatio, rect.y * yRatio, rect.width * xRatio, rect.height * yRatio)
  }
}

function stopVideoProcessing() {
  if (src != null && !src.isDeleted()) src.delete()
  if (dstC1 != null && !dstC1.isDeleted()) dstC1.delete()
  if (dstC3 != null && !dstC3.isDeleted()) dstC3.delete()
  if (dstC4 != null && !dstC4.isDeleted()) dstC4.delete()
}

function stopCamera() {
  if (!streaming) return
  stopVideoProcessing()
  document
    .getElementById('canvasOutput')
    .getContext('2d')
    .clearRect(0, 0, width, height)
  video.pause()
  video.srcObject = null
  stream.getVideoTracks()[0].stop()
  streaming = false
}

//function initUI() {
//  stats = new Stats()
//  stats.showPanel(0)
//  document.getElementById('container').appendChild(stats.dom)
//}

function opencvIsReady() {
  console.log('OpenCV.js is ready')
  //  initUI()
  startCamera()
}

async function tensorflow(
  leftrighteye,
  leftlefteye,
  leftface,
  face_grid_left,
  rightrighteye,
  rightlefteye,
  rightface,
  face_grid_right
) {
  console.log('model load')
  const model = await tf.loadLayersModel('testmodel/model.json')
  console.log('model.predict')
  output = await model.predict([
    leftrighteye,
    leftlefteye,
    leftface,
    face_grid_left,
    rightrighteye,
    rightlefteye,
    rightface,
    face_grid_right
  ])
  console.log('predict finish')
  const leftoutput = output[0].dataSync()
  const rightoutput = output[1].dataSync()
  //alert(leftoutput);
  console.log(leftoutput, rightoutput)
  leftdistance = Math.sqrt(leftoutput[0] ** 2 + leftoutput[1] ** 2)
  rightdistance = Math.sqrt(rightoutput[0] ** 2 + rightoutput[1] ** 2)
  console.log(leftdistance, rightdistance)
  if (leftdistance < 10 && rightdistance < 10) {
    console.log('save')
    document.getElementById('canvasOutput9').style.visibility = 'visible'
    const canvas = document.getElementById('canvasOutput9')
    const downloadLink = document.getElementById('download_link')
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()
    console.log(year, month, day, hour, minute, second)
    const filename = '' + year + '-' + month + '-' + day + '-' + hour + '-' + minute + '-' + second + '.png'
    console.log(filename)
    var download_button = document.getElementById('download_button')
    document.getElementById('download_button').style.visibility = 'visible'
    //alert('画像を保存できます')

    download_button.addEventListener('click', function () {
      downloadLink.href = canvas.toDataURL('image/png')
      downloadLink.download = filename
      downloadLink.click()
    })
  }
}

function loadScript(url, callback) {
  var script = document.createElement('script')
  script.type = 'text/javascript'
  script.src = url

  if (script.readyState) {
    script.onreadystatechange = function () {
      if (script.readyState === 'loaded' || script.readyState === 'complete') {
        script.onreadystatechange = null
        callback()
      }
    }
  } else {
    script.onload = function () {
      callback()
    }
  }

  document.getElementsByTagName('head')[0].appendChild(script)
}
